class AddColumnsToUserlogin < ActiveRecord::Migration[5.0]
  def change
    add_column :userlogins, :provider, :string
    add_column :userlogins, :uid, :string
    add_column :userlogins, :name, :string
    add_column :userlogins, :oauth_token, :string
    add_column :userlogins, :oauth_expires_at, :datetime
  end
end
