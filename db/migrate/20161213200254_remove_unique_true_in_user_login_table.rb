class RemoveUniqueTrueInUserLoginTable < ActiveRecord::Migration[5.0]
  def change
    remove_index :userlogins, :email
    add_index :userlogins, :email
  end
end
