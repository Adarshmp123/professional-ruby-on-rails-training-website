Rails.application.routes.draw do
  
  get 'auth/:provider/callback', to:'sessions#create'
  get 'auth/failure', to: redirect('/')
  get 'signout', to: 'sessions#destroy', as: 'signout'

 

  get '/alumni' => 'home#alumni'  
  get '/about' => 'home#about'
  get '/index' => 'home#index'  
    resources  :home
  root to: 'home#index'
  devise_for :userlogins
 
end


