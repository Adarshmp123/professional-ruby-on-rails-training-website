class Userlogin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
   devise :database_authenticatable, :registerable,
          :rememberable, :trackable, :validatable

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |userlogin|
      userlogin.provider = auth.provider 
      userlogin.uid      = auth.uid
      userlogin.name     = auth.info.name
      userlogin.oauth_token = auth.credentials.token
      userlogin.oauth_expires_at = Time.at(auth.credentials.expires_at)
      userlogin.save!
    end
  end  

  
end
