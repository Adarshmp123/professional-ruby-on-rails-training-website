class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)
    alumni_path
  end

private
  def current_user
    @current_user ||= Userlogin.find(session[:userlogin_id]) if session[:userlogin_id]
  end
  helper_method :current_user

end
