class SessionsController < ApplicationController
  def create
    userlogin = Userlogin.from_omniauth(env["omniauth.auth"])
    session[:userlogin_id] = userlogin.id
    redirect_to "/alumni"
  end
  def destroy
    session[:userlogin_id] = nil
    redirect_to "/index"
  end 
end
